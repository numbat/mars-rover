Mars Rover

To build (from this directory):
$ mvn clean package

To run demo (from this directory) using file input ("input.txt" provided):
$ java -jar target/rover-0.1.jar input.txt

