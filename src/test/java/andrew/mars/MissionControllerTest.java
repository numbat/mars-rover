package andrew.mars;

import andrew.mars.input.InputParsingException;
import andrew.mars.input.InputProvider;
import andrew.mars.rover.InvalidInstructionException;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class MissionControllerTest {

    @Test
    public void whenInputIsValidShouldExecuteSuccessfully() {
        List<String> testInput = new ArrayList<>();
        testInput.add("5 5");
        testInput.add("1 2 N");
        testInput.add("LMLMLMLMM");
        testInput.add("3 3 E");
        testInput.add("MMRMMRMRRM");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);

        Iterable<String> output = missionController.explore();

        Iterator<String> iterator = output.iterator();
        assertTrue(iterator.next().equals("1 3 N"));
        assertTrue(iterator.next().equals("5 1 E"));
    }

    @Test
    public void whenInputHasFiveRoversShouldExecuteSuccessfully() {
        List<String> testInput = new ArrayList<>();
        testInput.add("5 5");
        testInput.add("1 2 N");
        testInput.add("LMLMLMLMM");
        testInput.add("3 3 E");
        testInput.add("MMRMMRMRRM");
        testInput.add("0 0 E");
        testInput.add("RRRRLLLL");
        testInput.add("1 2 N");
        testInput.add("LML");
        testInput.add("5 5 S");
        testInput.add("MMLLRR");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);

        Iterable<String> output = missionController.explore();

        Iterator<String> iterator = output.iterator();
        assertTrue(iterator.next().equals("1 3 N"));
        assertTrue(iterator.next().equals("5 1 E"));
        assertTrue(iterator.next().equals("0 0 E"));
        assertTrue(iterator.next().equals("0 2 S"));
        assertTrue(iterator.next().equals("5 3 S"));
    }

    @Test
    public void whenInputHasNoRoversShouldExecuteSuccessfully() {
        List<String> testInput = new ArrayList<>();
        testInput.add("5 5");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        Iterable<String> output = missionController.explore();

        assertTrue(output.iterator().hasNext() == false);
    }

    @Test(expected = InputParsingException.class)
    public void whenInputHasNoBoundaryCoordinatesShouldThrowInputParsingException() {
        List<String> testInput = new ArrayList<>();
        testInput.add("1 2 N");
        testInput.add("LMLMLMLMM");
        testInput.add("3 3 E");
        testInput.add("MMRMMRMRRM");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        missionController.explore();
    }

    @Test(expected = InputParsingException.class)
    public void whenInputIsEmptyListShouldThrowInputParsingException() {
        List<String> testInput = new ArrayList<>();

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        missionController.explore();
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenInputIsNullShouldThrowIllegalArgumentException() {
        new MissionController(null);
    }

    @Test(expected = InputParsingException.class)
    public void whenInputHasEmptyBoundaryLineCoordinatesShouldThrowInputParsingException() {
        List<String> testInput = new ArrayList<>();
        testInput.add("");
        testInput.add("1 2 N");
        testInput.add("LMLMLMLMM");
        testInput.add("3 3 E");
        testInput.add("MMRMMRMRRM");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        missionController.explore();
    }

    @Test(expected = InputParsingException.class)
    public void whenInputHasInvalidRoverInputShouldThrowInputParsingException() {
        List<String> testInput = new ArrayList<>();
        testInput.add("1 1");
        testInput.add("1 2 3 N");
        testInput.add("LMLMLMLMM");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        missionController.explore();
    }

    @Test(expected = InputParsingException.class)
    public void whenInputHasExtraSpaceInRoverInputShouldThrowInputParsingException() {
        List<String> testInput = new ArrayList<>();
        testInput.add("1 1");
        testInput.add(" 1 2 N");
        testInput.add("LMLMLMLMM");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        missionController.explore();
    }

    @Test(expected = InvalidInstructionException.class)
    public void whenInputHasInvalidRoverInstructionsShouldThrowInputParsingException() {
        List<String> testInput = new ArrayList<>();
        testInput.add("5 5");
        testInput.add("1 2 N");
        testInput.add(" RRR");

        InputProvider mockInputProvider = Mockito.mock(InputProvider.class);
        Mockito.when(mockInputProvider.getInput()).thenReturn(testInput);

        MissionController missionController = new MissionController(mockInputProvider);
        missionController.explore();
    }
}
