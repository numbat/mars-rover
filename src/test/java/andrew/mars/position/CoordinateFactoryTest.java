package andrew.mars.position;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CoordinateFactoryTest {

    @Test
    public void whenGettingZeroZeroViaFactoryShouldGetCoordinateForZeroZero() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        Coordinate coordinate = coordinateFactory.getCoordinate(0, 0);

        assertTrue(coordinate.getX() == 0);
        assertTrue(coordinate.getY() == 0);
    }

    @Test
    public void whenGettingXYViaFactoryShouldGetCoordiateForXY() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        Coordinate coordinate = coordinateFactory.getCoordinate(2, 3);

        assertTrue(coordinate.getX() == 2);
        assertTrue(coordinate.getY() == 3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenNegativeXYShouldThrowIllegalArgumentException() {
        new CoordinateFactory(-1, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenNegativeXShouldThrowIllegalArgumentException() {
        new CoordinateFactory(5, -1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenNegativeYShouldThrowIllegalArgumentException() {
        new CoordinateFactory(-1, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenZeroXYShouldThrowIllegalArgumentException() {
        new CoordinateFactory(0, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenZeroXShouldThrowIllegalArgumentException() {
        new CoordinateFactory(0, 5);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenZeroYShouldThrowIllegalArgumentException() {
        new CoordinateFactory(5, 0);
    }

    @Test(expected = CoordinateAllocatedException.class)
    public void whenCoordinateAlreadyAllocatedShouldThrowCoordinateAllocatedException() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        coordinateFactory.getCoordinate(2, 2);
        coordinateFactory.getCoordinate(2, 2);
    }

    @Test(expected = CoordinateAllocatedException.class)
    public void whenGetCoordinateOutOfRangeXShouldThrowCoordinateAllocatedException() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        coordinateFactory.getCoordinate(6, 3);
    }

    @Test(expected = CoordinateAllocatedException.class)
    public void whenGetCoordinateOutOfRangeYShouldThrowCoordinateAllocatedException() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        coordinateFactory.getCoordinate(3, 6);
    }

    @Test(expected = CoordinateAllocatedException.class)
    public void whenGetCoordinateWithNegativeYShouldThrowCoordinateAllocatedException() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        coordinateFactory.getCoordinate(6, -1);
    }

    @Test(expected = CoordinateAllocatedException.class)
    public void whenGetCoordinateWithNegativeXShouldThrowCoordinateAllocatedException() {
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);
        coordinateFactory.getCoordinate(-3, 3);
    }
}
