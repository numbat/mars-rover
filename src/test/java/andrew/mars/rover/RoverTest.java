package andrew.mars.rover;

import andrew.mars.position.CoordinateFactory;
import andrew.mars.position.Direction;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RoverTest {

    @Test(expected = InvalidInstructionException.class)
    public void whenInstructionUnknownShouldThrowInvalidInstructionException(){
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);

        Rover rover = new Rover(coordinateFactory, 0, 0, Direction.N);
        rover.execute("ABCD");
    }

    @Test
    public void whenRoverCreatedShouldHaveInitialPositionAsCoordinate(){
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);

        Rover rover = new Rover(coordinateFactory, 0, 0, Direction.N);
        String roverPosition = rover.toString();
        assertTrue(roverPosition.equals("0 0 N"));
    }

    @Test
    public void whenRoverMovesNorthShouldHavePositionNorthOfCurrentCoordinate(){
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);

        Rover rover = new Rover(coordinateFactory, 0, 0, Direction.N);
        rover.execute("M");
        String roverPosition = rover.toString();
        assertTrue(roverPosition.equals("0 1 N"));
    }

    @Test(expected = InvalidInstructionException.class)
    public void whenRoverMovesOffGridShouldThrowCoordinateAllocationException(){
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);

        Rover rover = new Rover(coordinateFactory, 3, 0, Direction.S);
        rover.execute("M");
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenNullInstructionsShouldThrowIllegalArgumentException(){
        CoordinateFactory coordinateFactory = new CoordinateFactory(5, 5);

        Rover rover = new Rover(coordinateFactory, 3, 0, Direction.S);
        rover.execute(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenCoordinateFactoryShouldThrowIllegalArgumentException(){
        new Rover(null, 3, 0, Direction.S);
    }
}
