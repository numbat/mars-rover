package andrew.mars.input;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Example input provider implemented to read from file.
 */
public class FileInputProvider implements InputProvider {

    private final String path;

    public FileInputProvider(final String path) {
        this.path = path;
    }

    @Override
    public Iterable<String> getInput() {
        List<String> input = new ArrayList<>();
        try {
            Scanner scanner = new Scanner(new File(this.path));
            while (scanner.hasNextLine()) {
                input.add(scanner.nextLine());
            }
            scanner.close();
        } catch (IOException ioe) {
            throw new InputParsingException(ioe);
        }
        return input;
    }
}
