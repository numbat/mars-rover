package andrew.mars.input;

/**
 * Thrown if the input cannot be parsed.
 */
public class InputParsingException extends RuntimeException {

    public InputParsingException(String message) {
        super(message);
    }

    public InputParsingException(Throwable throwable){
        super(throwable);
    }

    public InputParsingException(String message, Throwable throwable){
        super(message, throwable);
    }
}
