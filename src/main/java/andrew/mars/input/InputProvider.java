package andrew.mars.input;

/**
 * Input provider to get the input that defines the plateau coordinates,
 * rovers and mars instructions.
 */
public interface InputProvider {

    /**
     * Get the input as a list of strings.
     *
     * @return the input for the application
     */
    Iterable<String> getInput();
}
