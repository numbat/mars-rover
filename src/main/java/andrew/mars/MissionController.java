package andrew.mars;

import andrew.mars.input.FileInputProvider;
import andrew.mars.input.InputParsingException;
import andrew.mars.input.InputProvider;
import andrew.mars.position.CoordinateFactory;
import andrew.mars.position.Direction;
import andrew.mars.rover.Rover;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Entry point for a Mars Exploration program.
 */
public class MissionController {

    private final InputProvider inputProvider;

    /**
     * Create a mission controller based on the given input that specifies
     * necessary parameters to instantiate rovers and move around the grid.
     *
     * @param inputProvider the provider that will provide the input for the program
     */
    public MissionController(final InputProvider inputProvider) {
        if (inputProvider != null) {
            this.inputProvider = inputProvider;
        } else {
            throw new IllegalArgumentException("Input parser cannot be null");
        }
    }

    /**
     * Explore Mars and print the output to the given print stream. The input assumes
     * the first line defines the boundaries of the explorable coordinates, followed by
     * pairs of input representing a rover's initial starting parameters and the
     * corresponding instructions for the rover.
     *
     * @return a list of accumulated output
     */
    public Iterable<String> explore() {
        Iterable<String> input = inputProvider.getInput();

        if (input == null || !input.iterator().hasNext()) {
            throw new InputParsingException("Input cannot be null or empty");
        }

        Iterator<String> iterator = input.iterator();
        CoordinateFactory coordinateFactory = getCoordinateFactory(iterator.next());

        List<String> output = new ArrayList<>();
        while (iterator.hasNext()) {
            Rover rover = getRover(iterator.next(), coordinateFactory);
            String instructions = iterator.next();
            rover.execute(instructions);
            output.add(rover.toString());
        }
        return output;
    }

    /**
     * Get the coordinate factory that will be used by the rovers.
     *
     * @param line the line of input specifying the boundaries of the coordinates
     * @return a coordinate factory based on the line f input
     */
    private CoordinateFactory getCoordinateFactory(final String line) {
        try {
            String[] boundaries = line.split(" ");
            if (boundaries.length == 2) {
                return new CoordinateFactory(Integer.valueOf(boundaries[0]), Integer.valueOf(boundaries[1]));
            } else {
                throw new InputParsingException("Expected two boundary values, but found " + boundaries.length);
            }
        } catch (NumberFormatException nfe) {
            throw new InputParsingException("Input has invalid boundary values", nfe);
        }
    }

    /**
     * Return a rover based on the given line of input specifying the starting x and y coordinates
     * and the direction.
     *
     * @param line              the line of input
     * @param coordinateFactory the coordinate factory the rover will use to generate its coordinates.
     * @return the rover created based on the line of input
     */
    private Rover getRover(final String line, final CoordinateFactory coordinateFactory) {
        try {
            String[] roverParameters = line.split(" ");
            if (roverParameters.length == 3) {
                return new Rover(
                        coordinateFactory,
                        Integer.valueOf(roverParameters[0]),
                        Integer.valueOf(roverParameters[1]),
                        Direction.valueOf(roverParameters[2].toUpperCase()));
            } else {
                throw new InputParsingException("Expected three rover parameters, but found " + roverParameters.length);
            }
        } catch (NumberFormatException nfe) {
            throw new InputParsingException("Input has invalid x and y values", nfe);
        } catch (IllegalArgumentException iae) {
            throw new InputParsingException(iae);
        }
    }

    /**
     * Example entry point using file input.
     *
     * @param args the first and only argument should be the file input
     */
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Incorrect number of arguments - expected 1 but was " + args.length);
            return;
        }

        InputProvider inputProvider = new FileInputProvider(args[0]);
        MissionController marsExploration = new MissionController(inputProvider);
        Iterable<String> output = marsExploration.explore();

        for (String out : output) {
            System.out.println(out);
        }
    }
}
