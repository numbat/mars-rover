package andrew.mars.rover;

import andrew.mars.position.Coordinate;
import andrew.mars.position.CoordinateAllocatedException;
import andrew.mars.position.CoordinateFactory;
import andrew.mars.position.Direction;

/**
 * A mars has a location (as a {@link Coordinate} and a {@link Direction}.
 */
public class Rover {

    private final CoordinateFactory coordinateFactory;

    private Coordinate coordinate;
    private Direction direction;

    /**
     * Create a mars using the starting parameters, and generating subsequent movement using the
     * given coordinate factory.
     *
     * @param coordinateFactory the coordinate factory used by the mars for moving around
     * @param startX            the x value of the rover start position
     * @param startY            the y value of the rover start position
     * @param direction         the mars's initial direction
     */
    public Rover(final CoordinateFactory coordinateFactory, final int startX, final int startY, final Direction direction) {
        if (coordinateFactory != null) {
            this.coordinateFactory = coordinateFactory;
            this.coordinate = this.coordinateFactory.getCoordinate(startX, startY);
            this.direction = direction;
        } else {
            throw new IllegalArgumentException("Coordinate Factory cannot be null");
        }
    }

    /**
     * Execute a packet of instructions. Instructions are sent to the rover as a string.
     *
     * @param instructionPacket the instruction packet
     */
    public void execute(final String instructionPacket) {
        if (instructionPacket != null) {
            char[] instructions = instructionPacket.toUpperCase().toCharArray();
            for (char instruction : instructions) {
                switch (instruction) {
                    case 'L':
                        this.turnLeft();
                        break;
                    case 'R':
                        this.turnRight();
                        break;
                    case 'M':
                        this.moveForward();
                        break;
                    default:
                        throw new InvalidInstructionException("Could not interpret instruction '" + instruction + "'");
                }
            }
        } else {
            throw new IllegalArgumentException("Instructions cannot be null");
        }
    }

    /**
     * Move the mars forward based on its current direction.
     */
    private void moveForward() {
        try {
            switch (direction) {
                case N:
                    coordinate = coordinateFactory.exchangeForOneNorth(coordinate);
                    break;
                case E:
                    coordinate = coordinateFactory.exchangeForOneEast(coordinate);
                    break;
                case S:
                    coordinate = coordinateFactory.exchangeForOneSouth(coordinate);
                    break;
                case W:
                    coordinate = coordinateFactory.exchangeForOneWest(coordinate);
                    break;
            }
        } catch (CoordinateAllocatedException cae) {
            throw new InvalidInstructionException("The rover could not move " + direction);
        }
    }

    /**
     * Turn left from the current direction.
     */
    private void turnLeft() {
        direction = direction.left;
    }

    /**
     * Turn right from the current direction.
     */
    private void turnRight() {
        direction = direction.right;
    }

    @Override
    public String toString() {
        return coordinate + " " + direction;
    }
}
