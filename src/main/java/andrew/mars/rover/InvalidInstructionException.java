package andrew.mars.rover;

/**
 * Thrown if the instruction could not be interpreted.
 */
public class InvalidInstructionException extends RuntimeException {

    public InvalidInstructionException(String message) {
        super(message);
    }
}
