package andrew.mars.position;

/**
 * Thrown by {@link CoordinateFactory} if the coordinate is already allocated.
 */
public class CoordinateAllocatedException extends RuntimeException {

    public CoordinateAllocatedException(final String message) {
        super(message);
    }

    public CoordinateAllocatedException(final int x, final int y) {
        super("The coordinate (" + x + ", " + ") could not be allocated.");
    }

    public CoordinateAllocatedException(final Coordinate coordinate) {
        super("The coordinate " + coordinate + " could not be allocated.");
    }
}
