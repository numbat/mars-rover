package andrew.mars.position;

import java.util.HashSet;
import java.util.Set;

/**
 * Coordinate factory manages the creation and allocation of coordinates. Factory will throw
 * {@link CoordinateAllocatedException} if there is an attempt to get a coordinate that is
 * already allocated.
 */
public class CoordinateFactory {

    private static final int MIN_X = 0;
    private static final int MIN_Y = 0;

    private final int maxX;
    private final int maxY;

    private final Set<Coordinate> allocatedCoordinates;

    /**
     * Create a coordinate factory with the given maximum values.
     *
     * @param maxX the maximum x value
     * @param maxY the maximum y value
     */
    public CoordinateFactory(final int maxX, final int maxY) {
        if (isPositive(maxX) && isPositive(maxY)) {
            this.maxX = maxX;
            this.maxY = maxY;
            this.allocatedCoordinates = new HashSet<Coordinate>();
        } else {
            throw new IllegalArgumentException("Boundary values for coordinates must be positive integers.");
        }
    }

    /**
     * Get a new coordinate, provided it is not yet taken.
     *
     * @param x the x value of the coordinate
     * @param y the y value of the coordinate
     * @return the new coordinate
     */
    public Coordinate getCoordinate(final int x, final int y) {
        if (isWithinBoundary(x, y)) {
            Coordinate coordinate = new CoordinateImpl(x, y);
            if (allocatedCoordinates.contains(coordinate)) {
                throw new CoordinateAllocatedException(coordinate);
            }
            allocatedCoordinates.add(coordinate);
            return coordinate;
        } else {
            throw new CoordinateAllocatedException(x, y);
        }
    }

    /**
     * Exchange the current coordinate for one coordinate to the north.
     *
     * @param currentCoordinate the current coordinate to take the bearing
     * @return the coordinate one to the north
     */
    public Coordinate exchangeForOneNorth(final Coordinate currentCoordinate) {
        return exchangeCoordinate(currentCoordinate, currentCoordinate.getX(), currentCoordinate.getY() + 1);
    }

    /**
     * Exchange the current coordinate for one coordinate to the west.
     *
     * @param currentCoordinate the current coordinate to take the bearing
     * @return the coordinate one to the west
     */
    public Coordinate exchangeForOneWest(final Coordinate currentCoordinate) {
        return exchangeCoordinate(currentCoordinate, currentCoordinate.getX() - 1, currentCoordinate.getY());
    }

    /**
     * Exchange the current coordinate for one coordinate to the east.
     *
     * @param currentCoordinate the current coordinate to take the bearing
     * @return the coordinate one to the east
     */
    public Coordinate exchangeForOneEast(final Coordinate currentCoordinate) {
        return exchangeCoordinate(currentCoordinate, currentCoordinate.getX() + 1, currentCoordinate.getY());
    }

    /**
     * Exchange the current coordinate for one coordinate to the south.
     *
     * @param currentCoordinate the current coordinate to take the bearing
     * @return the coordinate one to the south
     */
    public Coordinate exchangeForOneSouth(final Coordinate currentCoordinate) {
        return exchangeCoordinate(currentCoordinate, currentCoordinate.getX(), currentCoordinate.getY() - 1);
    }

    /**
     * Exchange the given coordinate for the new coordinate. This method attempts to
     * allocate the new coordinate first, before removing the existing allocation.
     *
     * @param existingCoordinate the existing coordinate to be exchanged
     * @param newX               the new X coordinate
     * @param newY               the new Y coordinate
     * @return the exchanged coordinate
     */
    private Coordinate exchangeCoordinate(final Coordinate existingCoordinate, final int newX, final int newY) {
        if (existingCoordinate == null) {
            throw new IllegalArgumentException("Existing coordinate cannot be null.");
        }
        Coordinate newCoordinate = getCoordinate(newX, newY);
        if (allocatedCoordinates.contains(existingCoordinate)) {
            allocatedCoordinates.remove(existingCoordinate);
        }
        return newCoordinate;
    }

    /**
     * Check if the value is greater than zero.
     *
     * @param value the value
     * @return true if greater than zero
     */
    private boolean isPositive(final int value) {
        if (value > 0) {
            return true;
        }
        return false;
    }

    /**
     * Ensure the given value is in the provided range (inclusive).
     *
     * @param value    the value to check
     * @param minValue the minimum range value
     * @param maxValue the maximum range value
     */
    private boolean isInRange(final int value, final int minValue, final int maxValue) {
        if (!(minValue < maxValue)) {
            throw new IllegalArgumentException("Invalid range (" + minValue + ", " + maxValue + ")");
        }
        if ((value >= minValue) && (value <= maxValue)) {
            return true;
        }
        return false;
    }

    /**
     * Check the given x and y values are within the boundaries of allowable
     * coordinate ranges for this factory.
     *
     * @param x the x value to check
     * @param y the y value to check
     */
    private boolean isWithinBoundary(final int x, final int y) {
        if ((isInRange(x, MIN_X, maxX) && isInRange(y, MIN_Y, maxY))) {
            return true;
        }
        return false;
    }
}
