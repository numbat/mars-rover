package andrew.mars.position;

/**
 * Represents an (x, y) value in a two-dimensional cartesian plane.
 */
public interface Coordinate {

    /**
     * Get the x value.
     *
     * @return the x value
     */
    int getX();

    /**
     * Get the y value.
     *
     * @return the y value
     */
    int getY();
}
