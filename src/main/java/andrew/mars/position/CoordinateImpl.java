package andrew.mars.position;

/**
 * Implementation of an immutable (x, y) coordinate.
 */
class CoordinateImpl implements Coordinate {
    private final int x;
    private final int y;

    public CoordinateImpl(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public int getX() {
        return x;
    }

    @Override
    public int getY() {
        return y;
    }

    @Override
    public String toString() {
        return x + " " + y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CoordinateImpl)) {
            return false;
        }

        CoordinateImpl that = (CoordinateImpl) o;

        if ((x != that.x) || (y != that.y)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }
}

