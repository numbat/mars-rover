package andrew.mars.position;

/**
 * Enumeration of four-way facing as based off compass directions, and
 * encapsulating the new direction based off an immediate 90 degree left
 * or right turn.
 */
public enum Direction {
    N, E, S, W;

    /**
     * The direction to the immediate left of this direction.
     */
    public Direction left;

    /**
     * The direction to the immediate right of this direction.
     */
    public Direction right;

    static {
        N.left = W;
        N.right = E;
        E.left = N;
        E.right = S;
        S.left = E;
        S.right = W;
        W.left = S;
        W.right = N;
    }
}
